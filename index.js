// console.log("Hello World")

// Array Methods
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);

// Mutator Methods
// push()
// return length
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

fruits.push("Mango");
console.log(`Mutated Array from push(): \n ${fruits}`);
console.log(fruits.length);

// Adding Multiple 
fruits.push("Avocado", "Guava");
console.log(`Mutated Array from push(): \n ${fruits}`);
console.log(fruits.length);

// pop()
// return removed item
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

let removedFruit = fruits.pop();
console.log(`Mutated Array from pop(): \n ${fruits}`);
console.log(removedFruit);
console.log(fruits.length);

console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

removedFruit = fruits.pop();
console.log(`Mutated Array from pop(): \n ${fruits}`);
console.log(removedFruit);
console.log(fruits.length);

// unshift()
// return length
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

let fruitsLength = fruits.unshift("Strawberry", "Melon");
console.log(`Mutated Array from unshift(): \n ${fruits}`);
console.log(fruitsLength);

// shift()
// return removed item
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

removedFruit = fruits.shift();
console.log(`Mutated Array from shift(): \n ${fruits}`);
console.log(removedFruit);
console.log(fruits.length);

// splice()
// return removed array
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

let updatedFruit = fruits.splice(1, 1, "Pineapple");
console.log(`Mutated Array from splice(): \n ${fruits}`);
console.log(updatedFruit);
console.log(fruits.length);

fruits.splice(3, 0, "Cherry");
console.log(`Mutated Array from splice(): \n ${fruits}`);
console.log(updatedFruit);
console.log(fruits.length);

// sort()
// return sorted array
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

fruits.sort();
console.log(`Mutated Array from sort(): \n ${fruits}`);
console.log(fruits.length);

// reverse()
// return reversed array
console.log(`Current Array fruits[]: \n ${fruits}`);
console.log(fruits.length);

fruits.reverse();
console.log(`Mutated Array from reverse(): \n ${fruits}`);
console.log(fruits.length);

// Non-Mutator Methods
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE", "PH"];

// indexOf()
// return index number if no match found -1 will return
console.log(countries.indexOf("PH"));
console.log(countries.indexOf("BR"));

console.log(countries.indexOf("PH", 2));

// lastIndexOf()
// return last index number if no match found -1 will return
console.log(countries.lastIndexOf("PH", 4));

// slice()
let sliceArrayA = countries.slice(2);
console.log(sliceArrayA);
console.log(countries);

// slice from specified index to another index
let sliceArrayB = countries.slice(1, 5);
console.log(sliceArrayB);

// slice starting from last element
let sliceArrayC = countries.slice(-3);
console.log(sliceArrayC);

// toString()
// return array as string separated by comma
let stringArray  = countries.toString();
console.log(stringArray);

// concat()
// return combined arrays
let taskArrayA = ["Drink HTML", "Eat JavaScript"];
let taskArrayB = ["Inhale CSS", "Breath SaSS"];
let taskArrayC = ["Get Git", "Be Node"];

let task = taskArrayA.concat(taskArrayB);
console.log(task);

// combined multiple arrays
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combined arrays with elements
let combinedTask = taskArrayA.concat("Smell Express", "Throw React");
console.log(combinedTask);

// join()
// return array separated 
let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join(" "));

// Iteration Methods
// foreach()
console.log(allTasks);
allTasks.forEach(task => {
    console.log(task)
});

let filteredTasks = [];

let Task = allTasks.forEach(task => {
    if (task.length > 10) {
        filteredTasks.push(task);
    }
    return filteredTasks;
});
console.log(Task);
console.log(filteredTasks);

// map()
// return new array
let numbers = [1, 2, 3, 4, 5];

const numberMap = numbers.map(function(number){
    return number * number;
});

console.log(`Original Array: `);
console.log(numbers);
console.log(`Result :`);
console.log(numberMap);

// every()
// return boolean

console.log(numbers);
let allValid = numbers.every(function(number){
    return (number < 6);
})
console.log(allValid);

// some()
console.log(numbers);
let someValid = numbers.some(function(number){
    return (number < 3);
})
console.log(someValid);

// filter()
// return new array
console.log(numbers);
let filteredValid = numbers.filter(function(number){
    return (number === 0);
})
console.log(filteredValid);

// include()
// return boolean

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// reduce
console.log(numbers);
let total = numbers.reduce(function(x, y){
    console.log(`This is the value of x: ${x}`);
    console.log(`This is the value of y: ${y}`);
    return x + y;
});
console.log(total);